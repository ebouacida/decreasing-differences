---
title: "Decreasing Differences"
subtitle: "Description of the Data"
author:
  - name: Elias Bouacida
    email: elias.bouacida@univ-paris8.fr
    affiliation:
      name: Université Paris 8
      address: France
  - name: Maya Jalloul
    email: m.jalloul@lancaster.ac.uk
    affiliation:
      name: Lancaster University
      address: United Kingdom
  - name: Renaud Foucart
    email: r.foucart@lancaster.ac.uk
    affiliation:
      name: Lancaster University
      address: United Kingdom
date: last-modified
date-format: long
format: pdf
---

# Files

- Description of AnonymousData.xlsx, AnonymousData.csv, CorrectAnswers.csv, L_Advice.csv and H_Advice.csv

We explain in this file the different columns of the data.

## Description of the data


| Column Name | Description |
|:--|:--------------|
| Treatment | Treatment with (a) or without (b) information on the adviser type |
| Order | Order in which the advisers are used. 1 means H adviser for the first round and L adviser for the second round. 2 means L adviser for the first round, H adviser for the second round. |
| number | Participant number ID |
| date| date at which the experiment happened | 
| 1a1 to 1a10 | Answers to the 10 questions of the first round before seeing the adviser answers. 1 means -2.4 ; 2 means -0.7 ; 3 means +0.7 and 4 means +2.4 (same for the three next items)  |
| 1b1 to 1b10 | Answers to the 10 questions of the first round after seeing the adviser answers. |
| 2a1 to 2a10 | Answers to the 10 questions of the second round before seeing the adviser answers. | 
| 2b1 to 2b10 | Answers to the 10 questions of the second round after seeing the adviser answers. |
| age | Bracket age of the participant, empty if no answer is given. |
| male | Declared gender of the participant, 1 for male, 0 for female, no data means no answer was given. |
| c1 to c5 | Answers to the questions about control |
| elo_brackets | Rapid Elo bracket in which the subjects belong, if "Unrated", it means that the participants has not been part in enough official tournaments to have an Elo rating. |
| elo_approx | Approximate Elo rating of the subjects. It is computed from the original data by averaging the Elo of players in the bracket. By convention, unrated player have an Elo of 1,000. |

: Sheet "data" in AnonymousData.xlsx or file AnonymousData.csv

| Column Name | Description |
|:---|:-----|
| 1_1 to 1_10 | Correct answers to the first round of questions |
| 2_1 to 2_10 | Correct answers to the second round of questions |

: Sheet "correct" in AnonymousData.xlsx or file CorrectAnswers.csv

| Column Name | Description |
|:-|:-----|
| 1_1 to 1_10 | Answers from the H adviser to the first round of questions |
| 2_1 to 2_10 | Answer from the H adviser to the second round of questions  | 

: Sheet "H_advice" in AnonymousData.xlsx or file H_Advice.csv

In total, 15 answers were correct for the H adviser.

| Column Name | Description |
|:-|:-----|
| 1_1 to 1_10 | Answers from the L adviser to the first round of questions |
| 2_1 to 2_10 | Answer from the L adviser to the second round of questions |

: Sheet "L_advice" in AnonymousData.xlsx or file L_Advice.csv

In total, 3 answers were correct for the L adviser.