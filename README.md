---
author:
    - Elias Bouacida
    - Renaud Foucart
    - Maya Jalloul
---

# Decreasing Differences

This repository contains the experimental instructions, the data collected and the code processing for the research paper *Decreasing Differences in Expert Evaluations: Evidence From a Field Experiment in Chess*.

# Organisation of the Repository

- The folder `Experiment`  contains all the experimental material (instructions for the different treatments and answer sheets.)
- The folder `Data` contains the data obtained from the experiment and the description of the data files.
- The folder `Code` contains the code used to anonymize the data, to produce the pre-registered analysis and the final paper (once the paper will be finished).




# Licence

In general, the data in this repository is available under the CC BY-SA 4.0 licence, whereas the code is under GPLv3 licence.
More details are given in the [Licence file](LICENCE).

