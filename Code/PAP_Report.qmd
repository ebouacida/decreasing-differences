---
title: "Decreasing Differences in Expert Evaluations: Evidence From a Field Experiment in Chess"
subtitle: "Pre-Registered Analysis"
date: last-modified
date-format: long
author:
  - name: Elias Bouacida
    email: elias.bouacida@univ-paris8.fr
    affiliation:
      name: Université Paris 8
      address: France
  - name: Maya Jalloul
    email: m.jalloul@lancaster.ac.uk
    affiliation:
      name: Lancaster University
      address: United Kingdom
  - name: Renaud Foucart
    email: r.foucart@lancaster.ac.uk
    affiliation:
      name: Lancaster University
      address: United Kingdom
format: 
  html:
    code-fold: true
    code-overflow: wrap
    code-tools: true
  # pdf:
  #   documentclass: scrartcl
  #   papersize: a4
  #   keep-tex: false
execute:
  echo: true
  cache: refresh
  warning: false
---



# Introduction

This is the report of the pre-registered analysis for the project *Decreasing Differences in Expert Advice: Evidence From a Field Experiment in Chess*. 
The pre-registration is available at <https://aspredicted.org/124_MSY>.

# Importing the Data

We setup the data reading by loading the libraries and preparing some functions.

```{r setup}
library(tidyverse)
library(readxl)
library(knitr)
library(viridis)
library(kableExtra)
library(lfe)
library(modelsummary)

library(infer)

colours <- viridis(2)

source(file.path(".", "Functions.R"))
```

We load the data for answers from the participants first.

```{r load-data}
data <- read_excel(file.path('..', 'Data', 'AnonymousData.xlsx'), sheet="data")
```

## Cleaning the Data

We rewrote some of the original variables, to anonymize the data.
For some participants, we had both their rapid Elo and their normal Elo.
We kept only the rapid Elo, and as for some participants, knowing the Elo would identify them, we have bracketed the Elo and replace their Elo by the average Elo of participants in the experiments in the bracket.
We lose a bit of precision but blur the real Elo of participants.
We think rapid Elo better corresponds to our setup where participants have on average 54 seconds to evaluate a position.
Note that we fix the Elo of players without Elo at 1,000, as it is the norm in chess for players without a rating (Elo rating starts at 1,000).



We now split the data into two tables.
One table, `individual` groups the variables that are fixed at the participant level.
The second table `positions` groups the observed choice of positions by participants.

```{r splitting-control-data}
individual <- data %>% 
  select(ID, age, male, date, c1:c5, elo_approx, elo_brackets)

positions <- data %>% 
  select(!c(c1, c2, c3, c4, c5, age, male, elo_brackets))
```

We make a tidy data of the `positions`: an observation is in this table a choice by a subject of a pawn advantage for a given position.
We then create new variables:

- `before_advice` is a dummy with value `TRUE` if the observation is before receiving the advice (which correspond to having an `a` in the name of the position).
- `part` divides the positions into the first ten (part one) and the last ten (part two).
- `position` indicates the position number in each part.
We then merge `part` and `position` into one variable positions varying from 1 to 20.
- `H_adviser` tells if the position received the advice from the high ability adviser (if `TRUE`) or the low ability one.
Notice that the advice is only received for observations where `before_advice` is `FALSE`.
- `known_adviser` replace the `Treatment` variable with a more explicit Boolean variable.

```{r tidy_positions}
positions <- positions %>%
  pivot_longer(`1a1`:`2b10`, names_to = "position_timing", 
               values_to = "answer") %>% 
  mutate(before_advice = grepl("a", position_timing, fixed = TRUE)) %>% 
  separate(position_timing, into = c("part", "position"), sep = "a|b",
           convert = TRUE) %>% 
  mutate(H_adviser = xor(Order == 2, part == 1),
         position = (part - 1 )*10 + position,
         part = NULL,
         Order = NULL,
         known_adviser = (Treatment == "a"),
         Treatment = NULL,
         position_timing = NULL,
         answer = case_match(answer,
           1 ~ -2.4,
           2 ~ -0.7,
           3 ~ 0.7,
           4 ~ 2.4
         )
         ) %>% 
  relocate(answer, .after = last_col()) %>% 
  relocate(position, .before = answer)
```

## Exclusions

As the main object of analysis is the difference between the answer before and after the advice, we exclude from the analysis one participant who did not give any answer after receiving the advice as we have no observation for them.


```{r exclusion}
#| label: tab-exclusion
#| tbl-cap: "We exclude from the analysis participants who did not give an answer after receiving the advice."
excluded <- positions %>% 
  filter(!before_advice) %>% 
  group_by(ID) %>% 
  count(na = is.na(answer)) %>% 
  filter(na, n == 20)
  
positions <- positions %>% 
  filter(ID != excluded$ID)

individual <- individual %>% 
  filter(ID != excluded$ID)

excluded %>% kable()

```

## Advisers' answers

We also load the correct answers, and the advisers' answers.

```{r advisers_answers}
correct <- read_excel(file.path('..', 'Data', 'AnonymousData.xlsx'), 
                      sheet="correct")
H_advice <- read_excel(file.path('..', 'Data', 'AnonymousData.xlsx'), 
                       sheet="H_advice")
L_advice <- read_excel(file.path('..', 'Data', 'AnonymousData.xlsx'), 
                       sheet="L_advice")
```

We make them tidy so that it is easy to join them with the answers of participants.

```{r tidy_advice}
correct_eval <- tidy_advisers(correct, correct_eval, "correct_eval")
H_advice <- tidy_advisers(H_advice, H_advice, "H_advice")
L_advice <- tidy_advisers(L_advice, L_advice, "L_advice")
```

Finally, we join them with the answers given by participants earlier.

```{r join}
positions <- left_join(positions, correct_eval, by = "position")
positions <- left_join(positions, H_advice, by = "position")
positions <- left_join(positions, L_advice, by = "position")
```

## Separating by Elo

We separate participants into two groups.
The `high_elo` group is the group of subjects above the median rapid Elo rating.
We have `r nrow(individual)` participants, which is even, so that the two groups are of the same size.


```{r high-elo}
median_elo <- median(individual$elo_approx)

positions <- positions %>% 
  mutate(high_elo = elo_approx >= median_elo)

individual <- individual %>% 
  mutate(high_elo = elo_approx >= median_elo)
```
The average Rapid Elo rating of participants who have an Elo rating is `r round(mean(individual[individual$elo_brackets != "Unrated",]$elo_approx, na.rm = TRUE))`.
The median rapid Elo is around `r median_elo`.

# Decreasing Differences

First, we give in @tbl-adviser-correct the rate fo correct answers for each adviser.


```{r adviser-correct}
#| label: tbl-adviser-correct
#| tbl-cap: "Percentage of correct answer for each adviser"
positions %>% 
  summarise(
    H = mean(correct_eval == H_advice),
    L = mean(correct_eval == L_advice)
    ) %>% 
  mutate(across(where(is.numeric), percentage)) %>%
  kable()
```

Before we can test if decreasing differences happen, we need to compute the rate of correct answers for each type of participants.
`distance_correct` compute the distance between the correct answer and the answer given by subjects, in pawn advantage.
If the distance is 0, the answer is correct.
Otherwise, if the distance is different from the zero or no answer was given, the answer is incorrect.

```{r correct_answers}
positions <- positions %>% 
  mutate(distance_correct = (answer - correct_eval),
    correct_ans = case_when(
      distance_correct == 0 ~ TRUE,
      .default = FALSE # <1>
      )
  )
```
1. NA's are wrong answers

## NAM and PAM

We now test decreasing difference in two different ways.
First, when comparing H advice with L advice, then by comparing H advice with no advice.

To do that, we now have to compare answers before and after receiving the advice, so that an observation is not an answer for a position, but two answers for a position: before and after receiving the advice.
In `positions_ba`, an observation is two answers: for a given position and a given subject, the answer before and after receiving the advice.
One of the main questions is the behavior towards receiving the advice and the benefit provided by the advice. 
We also look at kept answers.

```{r before-after}
positions_ba <- positions %>% 
  ungroup() %>% 
  mutate(timing = if_else(before_advice, "before", "after"),
         before_advice = NULL) %>% 
  pivot_wider(names_from = timing, 
              values_from = c(answer, distance_correct, correct_ans)) %>% 
  mutate(keep = case_when(
    answer_before == answer_after ~ TRUE, 
    is.na(answer_before) & is.na(answer_after) ~ TRUE, # <1>
    .default = FALSE)
    )
```
1. If you have not answered before and you have not answered after, you kept your non-answer.

### With L Advice

First, we create a Boolean variable `nam` telling if participants and advisers are negatively assortatively matched or positively so.
We then compute the proportion of correct answers after receiving the advice in each treatment, separating by assortativity.

```{r dd}
#| label: tbl-dd-Ladvice
#| tbl-cap: "Proportion of correct answers with positive and negative assortative matching in both treatments, with L advice."


positions_ba <- positions_ba %>% 
  ungroup() %>% 
  mutate(am = if_else((high_elo & !H_adviser) | (!high_elo & H_adviser),
                      "NAM", "PAM"))
  
share_all <- positions_ba %>% 
  group_by(am) %>% 
  summarise(correct = mean(correct_ans_after)) %>% 
  pivot_wider(names_from = am, values_from =correct) %>% 
  mutate(treatment = "All",
         .before = NAM)

pval_all <- positions_ba %>% 
  prop_test(correct_ans_after ~ am, order = c("NAM", "PAM")) %>%
  mutate(treatment = "All") %>% 
  select(c(treatment, p_value))

share_treatment <- positions_ba %>% 
  group_by(am, known_adviser) %>% 
  summarise(correct = mean(correct_ans_after)) %>% 
  pivot_wider(names_from = am, values_from = correct) %>% 
  mutate(treatment = if_else(known_adviser, "Known Adviser", "Unknown Adviser"),
         known_adviser =NULL, .before = NAM)

pval_known <- positions_ba %>% 
  filter(known_adviser) %>% 
  prop_test(correct_ans_after ~ am, order = c("NAM", "PAM")) %>%
  mutate(treatment = "Known Adviser") %>% 
  select(c(treatment, p_value))

pval_unknown <- positions_ba %>% 
  filter(!known_adviser) %>% 
  prop_test(correct_ans_after ~ am, order = c("NAM", "PAM")) %>%
  mutate(treatment = "Unknown Adviser") %>% 
  select(c(treatment, p_value))

namvspam <- rbind(share_all, share_treatment) %>% 
  mutate(
    pvalue = pvalue(c(pval_all$p_value,pval_unknown$p_value, pval_known$p_value))
    )


namvspam %>% 
  mutate(across(c(NAM, PAM), function(x) percentage(x))) %>% 
  kable(
    col.names = c("Treatment", "NAM", "PAM", 
                  paste0("P-value", footnote_marker_number(1))),
    booktabs = TRUE,
    align = "lccc",
    escape = FALSE#,
    # format = "latex"
  ) %>% 
  footnote(
    number = c("P-value of the test of proportion being equal, using a 
               chi-squared two-sided two sample test (aka a proportion test)."),
    threeparttable = TRUE
  )
```

In @tbl-dd-Ladvice, we see that negative assortative matching performs better than positive assortative matching, but not significantly so.


### Without Advice

We repeat exactly the same exercise as in the previous section, but now comparing the H advice with no advice (so using the answer of those receiving the L advice before they received it).

```{r dd-no-advice}
#| label: tbl-dd-noadvice
#| tbl-cap: "Proportion of correct answers with positive and negative assortative matching in both treatments, without L advice."
positions <- positions %>% # <1>
  mutate(am = if_else((high_elo & !H_adviser) | (!high_elo & H_adviser),
                      "NAM", "PAM"))

share_all_none <- positions %>% 
  filter(!before_advice & H_adviser | before_advice & !H_adviser) %>% 
  group_by(am) %>% 
  summarise(correct = mean(correct_ans)) %>% 
  pivot_wider(names_from = am, values_from =correct) %>% 
  mutate(treatment = "All",
         .before = NAM)

pval_all_none <- positions %>% 
  filter(!before_advice & H_adviser | before_advice & !H_adviser) %>% 
  prop_test(correct_ans ~ am, order = c("NAM", "PAM")) %>%
  mutate(treatment = "All") %>% 
  select(c(treatment, p_value))


share_treatment_none <- positions %>% 
  filter(!before_advice & H_adviser | before_advice & !H_adviser) %>% 
  group_by(am, known_adviser) %>% 
  summarise(correct = mean(correct_ans)) %>% 
  pivot_wider(names_from = am, values_from =correct) %>% 
  mutate(treatment = if_else(known_adviser, "Known adviser", "Unknown adviser"),
         known_adviser =NULL, .before = NAM)



pval_known_none <- positions %>% 
  filter(!before_advice & H_adviser | before_advice & !H_adviser) %>% 
  ungroup() %>% 
  filter(known_adviser) %>% 
  prop_test(correct_ans ~ am, order = c("NAM", "PAM")) %>%
  mutate(treatment = "Known adviser") %>% 
  select(c(treatment, p_value))

pval_unknown_none <- positions %>% 
  filter(!before_advice & H_adviser | before_advice & !H_adviser) %>% 
  ungroup() %>% 
  filter(!known_adviser) %>% 
  prop_test(correct_ans ~ am, order = c("NAM", "PAM")) %>%
  mutate(treatment = "Unknown adviser") %>% 
  select(c(treatment, p_value))

namvspam_none <- rbind(share_all_none, share_treatment_none) %>% 
  mutate(pvalue = pvalue(c(pval_all_none$p_value, 
                           pval_unknown_none$p_value, pval_known_none$p_value)),
         across(c(NAM, PAM), function(x) percentage(x)))

namvspam_none %>% 
  kable(
    col.names = c("Treatment", "NAM", "PAM", 
                  paste0("P-value", footnote_marker_number(1, "html"))),
    booktabs = TRUE,
    align = "lccc",
    escape = FALSE#,
    # format = "latex",
  ) %>% 
  footnote(
    number = c("P-value of the test of proportion being equal, using a 
               chi-squared two-sided two sample test (aka proportion test)."),
    threeparttable = TRUE
  )
```
1. We keep the positions rather than positions_ba for this analysis, as we mix and match answers before and after.

The results are qualitatively the same in @tbl-dd-noadvice: negative assortative matching performs better, but the differences are not significant. 
Overall, we do not observe decreasing differences.
The rate of correct answers is slightly higher without advice than with the low advice, showing that the L advice actually hurts participants.

### Note on Statistical Power


The results are not significant here.
One possible reason is the relatively small magnitude of the effect.
To quantify this idea, we look at the sample size we needed to detect the effect we observe.
We do that in the Unknown Adviser treatment with the L advice, which has the largest effect.


```{r stat-power}
power_data <- share_treatment %>% 
  filter(treatment == "Unknown Adviser")

power_data2 <- positions %>% 
  filter(!before_advice, !known_adviser) %>% 
  drop_na(correct_ans)

sample_size <- power.prop.test(p1 = power_data$PAM, p2 = power_data$NAM, power = 0.8) 

prop_size <- power.prop.test(n = nrow(power_data2), p1 = power_data$PAM, power = 0.8) 

prop_size2 <- power.prop.test(n = 1000, p1 = power_data$PAM, power = 0.8) 
```

Everything else equal, to detect a significantly different proportion between NAM (`r  percentage(sample_size$p1, digits = 2)`) and PAM (`r percentage(sample_size$p2, digits = 2)`), we needed a sample size of `r round(sample_size$n)`, whereas we aimed at a sample size of 1,000 and our realized sample size is `r nrow(power_data2)`.

The other way to look at the statistical power issue is to see what effect size we can measure with the sample size we have.
With the proportion of NAM we currently have, we would be able to detect a significant effect for a proportion with PAM at `r percentage(prop_size$p2, digits = 2)`.
With the sample size we aimed for, we would have been able to detect a proportion with PAM of `r percentage(prop_size2$p2, digits = 2)`.

# Impact of L-adviser

We now turn to the additional pre-registered analysis: the impact of the low quality adviser, by treatment.


```{r value-L}
#| label: tbl-value-Ladvice
#| tbl-cap: "Proportion of correct evaluation before and after receiving the L advice, by treatment."
pval_known <- positions %>% 
  filter(!H_adviser, known_adviser) %>% 
  prop_test(correct_ans ~ before_advice) %>% 
  select(p_value) %>% 
  mutate(Treatment = "Known adviser")

pval_unknown <- positions %>% 
  filter(!H_adviser, !known_adviser) %>% 
  prop_test(correct_ans ~ before_advice) %>% 
  select(p_value) %>% 
  mutate(Treatment = "Unknown adviser")

pval <- rbind(pval_known, pval_unknown)

ladvice <- positions %>% 
  filter(!H_adviser) %>% 
  group_by(before_advice, known_adviser) %>% 
  summarise(
    correct = mean(correct_ans, na.rm = TRUE)
  ) %>% 
  ungroup() %>% 
  mutate(Treatment = if_else(known_adviser, "Known adviser", "Unknown adviser"),
         known_adviser = NULL,
         before_advice = if_else(before_advice, "Before", "After")) %>% 
  pivot_wider(values_from = correct, names_from = before_advice) %>% 
  relocate(After, .after = Before)

left_join(ladvice, pval, by = "Treatment") %>% 
  mutate(p_value = pvalue(p_value),
         across(c(After, Before), function(x) percentage(x))) %>% 
  kable(
    align = "lccc",
    col.names = c("Treatment", "Before Advice", "After Advice", 
                  paste0("P-value", footnote_marker_number(1))),
    booktabs = TRUE,
    escape = FALSE
    # format = "latex"
  ) %>% 
  footnote(
    number = c("P-value of the test of proportion being equal, 
               using a chi-squared two-sided two sample test."),
    threeparttable = TRUE
  )
```

@tbl-value-Ladvice shows that the L adviser has a negative impact on the rate of correct answer, but with the sample and advice we have, the impact is not significant. 
Reassuringly, the impact seems to be slightly smaller when participants know that the advice is coming from the L adviser.

# Improvement in Median Prediction

First, we compute the median distance from the correct answer by position and treatment.

```{r med-prediction}
#| label: tbl-median-dist
#| tbl-cap: "Average of the median distance to the correct answer before and after receiving the advice, across all positions, by treatment."

median_distance <- positions %>% 
  group_by(position, before_advice, known_adviser) %>% 
  summarise(median_distance = median(distance_correct, na.rm = TRUE)) %>% 
  mutate(Treatment = if_else(known_adviser, "Known Adviser", "Unknown Adviser"),
       known_adviser = NULL,
       before_advice = if_else(before_advice, "Before", "After"))

median_distance %>% 
  pivot_wider(values_from = median_distance, names_from = before_advice) %>% 
  relocate(After, .after = Before) %>% 
  relocate(Treatment, .before = position) %>% 
  arrange(Treatment, position) %>%
  ungroup() %>% 
  group_by(Treatment) %>% 
  summarise(
    mean_before = mean(abs(Before)),
    mean_after = mean(abs(After))
  ) %>% 
  kable(
    col.names = c("Treatment", "Before", "After"),
    booktabs = TRUE,
    align = "lcc"
  )

```

```{r plot-median-distance}
#| label: fig-median
#| fig-cap: "Median distance to the correct answer before and after receiving the advice, by treatment."
ggplot(median_distance) +
  geom_bar(aes(x=median_distance, fill = before_advice, 
               y = after_stat(count) / 20), position = "dodge") +
  scale_fill_viridis("Advice", discrete = TRUE) +
  scale_y_continuous("Proportion of Postions", labels = scales::label_percent()) +
  scale_x_discrete("Median Distance (in Pawn Advantage)") +
  facet_wrap(vars(Treatment)) +
  theme_minimal()
```

@tbl-median-dist shows that there is an average improvement in the evaluation made by participants, especially when they know who the adviser is.
We plot the median distance in @fig-median to see the improvement disaggregated.
Knowing the adviser improves the evaluations of participants.
In particular, the median participant would choose the correct evaluation in more than 50% of the evaluations after receiving advice when knowing who the adviser is. 
It is not the case in any other situation.

# Regression Analysis

## Preference for Control Questions

We plot in @fig-control-questions the histogram of answers to the control questions.
The questions were:

1. I try to avoid situations where someone else tells me what to do.
2. I prefer to be a leader rather than a follower.
3. I enjoy making my own decisions.
4. I would rather someone else took over the leadership role when I am involved in a group project.
5. There are many situations in which I would prefer only one choice rather than having to make a decision.

The answers are rated on a 5-point Likert scale: Strongly disagree, Disagree, Neither agree nor disagree, Agree, Strongly agree, encoded from 1 to 5.
The first three questions give a preference for control, whereas the last two a dislike for control.
To have the rankings going in the same direction (1: low preference for control to 5: strong preference for control), we remap the answers of questions 4 and 5 to 6 minus their values.
We create a control index `control_index` which is the average value of the answers to the five control questions.


```{r control-index}
individual <- individual %>% 
    mutate(
    c5 = 6 - c5 ,
    c4 = 6 - c4
  ) %>% 
  rowwise() %>% 
  mutate(control_index = mean(c_across(c1:c5)))
```

```{r control-plot}
#| label: fig-control-questions
#| fig-cap: "Control question answers"
individual %>% 
  pivot_longer(cols = c1:c5, names_to = "control_questions", values_to = "answers") %>% 
  ggplot(aes(x = answers, fill = control_questions, y = after_stat(count) / 102)) +
  geom_bar(position = "dodge") +
  scale_y_continuous("Proportion of Participants", labels = scales::label_percent()) +
  scale_fill_viridis("Question", discrete = TRUE, labels = 1:5)
```
@fig-control-questions shows that over most questions, participants veer toward some preference for control.
`r sum(is.na(individual$control_index))` participants did not answer all the questions and are not shown here.


## Correct Answers


Finally, we look at the factors influencing the share of correct answers in both treatments in @tbl-regression-correct.
The errors are clustered at the position level.
In both cases, the reference level is the high Elo subjects receiving advice from the low quality adviser.
In the two right most regression, we replace the dummy `low elo` by the Elo of the participants to get more statistical power, because the Elo is now used as a continuous numerical variable, compared to the dummy `low Elo`.
We see that subjects with higher Elo give all else equal better answers.
The good adviser has a positive impact as well, in particular when it is known that the advice comes from him.

```{r regression-data}
positions_ba <- positions_ba %>% 
  mutate(rated = elo_approx != 1000,
         low_elo = !high_elo)

positions_ba <- left_join(positions_ba, individual, 
                          by = c("ID", "elo_approx", "high_elo", "date"))
```


```{r reg-correct}

simple_known <- felm(correct_ans_after ~ H_adviser * low_elo + male + age 
                     + control_index | position | 0 |  position,
                     positions_ba[positions_ba$known_adviser, ])

simple_unknown <- felm(correct_ans_after ~ H_adviser * low_elo + male + age
                       + control_index | position | 0 | position, 
                       positions_ba[!positions_ba$known_adviser, ])

exact_known <- felm(correct_ans_after ~ H_adviser  * elo_approx + male + age
                    + control_index | position | 0 |  position, 
                    positions_ba[positions_ba$known_adviser, ])

exact_unknown <- felm(correct_ans_after ~ H_adviser  * elo_approx + male + age 
                      + control_index | position | 0 | position,
                      positions_ba[!positions_ba$known_adviser, ])

```

```{r regression-summary}
#| label: tbl-regression-correct
#| tbl-cap: "Regression for the share of correct answers, with fixed effect at the position level."

models <- list(
  "Known" = simple_known,
  "Unknown" = simple_unknown,
  "Known" = exact_known,
  "Unknown" = exact_unknown
)

coef_names <- c(
  "low_eloTRUE" = "Low Elo",
  "elo_approx" = "Elo",
  "maleTRUE" = "Male",
  "H_adviserTRUE" = "H adviser",
  "control_index" = "Control Index"
)
options(modelsummary_format_numeric_latex = "plain")

modelsummary(models,
             coef_rename = coef_names,
             gof_omit = "IC|RMSE",
             estimate = "{estimate} ({p.value})",
             fmt = fmt_statistic("estimate"= fmt_significant(2), 
                                 "std.error" = fmt_significant(2), 
                                 "p.value" = pvalue),
             output = "kableExtra",
             notes = c(
               "Note: Robust standard errors clustered at the position level.", 
               "In parenthesis on the same line are the p-value, 
               below the standard error."
             )
             ) %>% 
  add_header_above(c("", "Grouped by Elo" = 2, "Approx. Elo"=2))
```

In @tbl-correct-all, we look at both treatments together.
For the regression with group by Elo, the baseline is the same as in the previous regressions, except that it is in the case of unknown adviser.
We can see in particular that the mere fact that it is the good adviser significantly enhance the share of correct answers with exact Elo.
The interaction term between the H adviser and knowing who the adviser is is not significant however.

```{r reg-correct-all-elo}
simple_all <- felm(correct_ans_after ~ H_adviser * low_elo * known_adviser 
                   + male + age + control_index |  position  | 0 | position, 
                   positions_ba)

exact_all <- felm(correct_ans_after ~ H_adviser * elo_approx * known_adviser 
                  + male + age + control_index | position | 0 | position,
                  positions_ba)
```

```{r reg-correct-all-elo}
#| label: tbl-correct-all
#| tbl-cap: "Regression model for the correct answer in both treatments together."


models_all <- list(
  "Simple" = simple_all,
  "Exact" = exact_all
)

coef_names <- c(
  coef_names,
  c(known_adviserTRUE = "Known adviser"),
  recursive = TRUE
)
options(modelsummary_format_numeric_latex = "plain")

modelsummary(models_all,
             coef_rename = coef_names,
             gof_omit = "IC|RMSE",
             estimate = "{estimate} ({p.value})",
             fmt = fmt_statistic("estimate"= fmt_significant(2), 
                                 "std.error" = fmt_significant(2), 
                                 "p.value" = pvalue),
             output = "kableExtra",
             notes = c(
               "Note: Robust standard errors clustered at the position level.", 
               "In parenthesis on the same line are the p-value, 
               below the standard error."
             )
             ) 
```