---
title: "Decreasing Differences in Expert Evaluations: Evidence From a Field Experiment in Chess"
subtitle: "Description of the Experimental files"
date: last-modified
date-format: long
author:
  - name: Elias Bouacida
    email: elias.bouacida@univ-paris8.fr
    affiliation:
      name: Université Paris 8
      address: France
  - name: Maya Jalloul
    email: m.jalloul@lancaster.ac.uk
    affiliation:
      name: Lancaster University
      address: United Kingdom
  - name: Renaud Foucart
    email: r.foucart@lancaster.ac.uk
    affiliation:
      name: Lancaster University
      address: United Kingdom
---

# Overview

This folder contains the experimental instructions and answer sheets for the 4 treatments of the experiment.
All the experiment was done with pen and paper, so no code is part of this repository.
The experimental instructions were given step by step.
The files are structured as follow `Xn_Type.pdf`, where `X` refers to the treatment, `n` to the order and `Type` to whether it is the instructions or the answer sheet.

## Treatments

There are two treatments in the experiment: either players knew the adviser rating, which is symbolized by letter `A`, or they did not (letter `B`).
The 20 answers asked were asked in two different orders, either the rated adviser first (number `1`) or the unrated adviser first (number `2`).
In the unknown adviser treatment, players do not know this, of course.

# Experiment

We ran the experiment during the Summer of 2023 in several cities in Lebanon, alongside tournaments organised by a local academy.^[The exact dates are August 15, August 20, September 2, and September 17, 2023. 
In line with the pre-registration, we stopped recruiting participants when we reached 100 subjects, so that we recruited a total of $103$ subjects.
Our total sample is however $n=102$ as, in line with our pre-registration, we removed observations for which no choice were made and one of our subjects did not write anything in the second part of the answer sheet. 
The project has received IRB approval from Lancaster University.] 
Our subjects were regular participants in tournaments, and had therefore a certain level of expertise in the game.
All subjects received the experimental material written both in English and Arabic.

## Protocol

We recruited subjects before the tournament through the organizing chess academy and paid for their registration (around \$5) as a participation fee.
The experiment took part in a separate room at times where our subjects were not playing. 
Each subject was randomly allocated either to a treatment with or without information on the adviser. 
Subjects received tasks booklets and answer sheets upon being seated.
There were two rounds of tasks, each corresponding to solving ten positions.
In each round, subjects were given 8 minutes to complete their evaluation of each position among the possible choices ($-2.4, -0.7, +0.7$, or $+2.4$) on the left part of their answer sheets. 
Then, they were provided with the answers of one of our two advisers for the same questions.
They were given 4 minutes to look back at their answers, compare with the advice, and complete the right part of the answer sheet with their possibly updated evaluations. 

In the known adviser condition, we told subjects that the answers we gave them were coming from "a player with a rating of 2335" for the round in which their adviser was $H$, and from "an unrated player, who plays regularly for fun" when their adviser was $L$. In the unknown adviser condition, we told them that "With equal probability, the player has a rating of 2335, or it is an unrated player, who plays regularly for fun".

After solving the two rounds of evaluations, subjects completed a short demographic questionnaire as well as questions about their stated preference for control (5 questions borrowed from @burger1979desirability).
All the sessions were administered by one of the co-authors of this study (Maya Jalloul), who read the experimental material and ensured no one could cheat. 

On top of the participation fee, we picked one of the 40 evaluations of each subject at random (20 evaluations before advice, and 20 after) and paid a variable amount of \$10 if the answer was correct.^[Given the difficult banking situation in Lebanon and the fact that some of our subjects were minor, we did not pay subjects directly in cash but with monetary vouchers for subsequent tournaments or other spending on the day.]
We only knew the subject number, and not their identity. 
We communicated a list of payments and subject numbers to the organizing chess academy, who then processed the payments based on a list they made allocating participant numbers to individuals.

## Instruction and Answer Files

Known adviser treatment:

- [A1_Instructions](A1_InstructionsKnownAdviser.pdf)
- [A2_Instructions](A2_InstructionsKnownAdviser.pdf)
- [A1_AnswerSheet](A1_AnswerSheet.pdf)
- [A2_AnswerSheet](A2_AnswerSheet.pdf)

Unknown adviser treatment:

- [B1_Instructions](B1_InstructionsUnknownAdviser.pdf)
- [B2_Instructions](B2_InstructionsUnknownAdviser.pdf)
- [B1_AnswerSheet](B1_AnswerSheet.pdf)
- [B2_AnswerSheet](B2_AnswerSheet.pdf)

# References
